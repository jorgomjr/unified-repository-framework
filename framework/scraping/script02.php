<?php
require '../simple_html_dom/simple_html_dom.php';
require '../tasks/Task.php';
require '../tasks/Pessoa.php';
require '../tasks/Normalize.php';

$p = new Pessoa();
$p->fonte = "www.google.com";
$p->imagem = "caminho da imagem";
$p->nome = "Nome da Pessoa";
$p->mais_caracteristicas = "                Testo Aleatório    ";

$normalize = new Normalize();
$normalize->processing($p);

print_r($p);