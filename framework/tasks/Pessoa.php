<?php
class Pessoa{
  public $nome; 
  public $apelido;
  public $datanasc;
  public $sexo;
  public $imagem;
  public $idade;
  public $cidade;
  public $estado;
  public $altura;
  public $peso;
  public $pele;
  public $cor_cabelo;
  public $cor_olho;
  public $mais_caracteristicas;
  public $data_desaparecimento;
  public $local_desaparecimento;
  public $circunstancia_desaparecimento;
  public $data_localizacao;
  public $dados_adicionais;
  public $situacao;
  public $fonte;
}
