<?php

class Normalize implements Task
{
  public function processing(Pessoa $p){
    $this->normalizer($p);

  }
  protected function normalizer(Pessoa $p){
    foreach ($p as &$item) {
      if ($p->fonte == $item || $p->imagem == $item)
      continue;
      $item = html_entity_decode(trim(strtolower($item)));
    }
  }
}
