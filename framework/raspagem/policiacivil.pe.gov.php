<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.


//composer TesseractOCR + Imagick

require_once '../vendor/autoload.php';
use thiagoalessio\TesseractOCR\TesseractOCR;
include "../simple_html_dom/simple_html_dom.php";


$url = "http://www.policiacivil.pe.gov.br/";
$tmpfile = tempnam ("/tmp", "");
$tmpfile2 = tempnam ("/tmp", "");

$html = file_get_html($url."index.php/criancas-desaparecidas.html");
$table =$html->find('td[valign=top] table.contentpaneopen ',1);

foreach ($table->find('td[valign=top] table[style=width: 96%; border: 0px solid #000000;] a') as $people){

    $cmd = "wget -O $tmpfile " . '"' . $url . $people->href . '"';
    exec($cmd);
    if(filesize($tmpfile) != 0){
        $im = new Imagick();
        $im->setResolution(1000, 1000);
        $im->readImage($tmpfile);
        $im->setImageFormat('png');

        $im->writeImages($tmpfile2, false);
        $im->clear();
        $im->destroy();

        $TC = new TesseractOCR($tmpfile2);
        $result = $TC->lang('por')->run();
        print_r(array_filter(explode("\n", $result)));
    }

unlink($tmpfile);
unlink($tmpfile2);

}
*/
include '../vendor/autoload.php';
include "../simple_html_dom/simple_html_dom.php";
include 'atualizacaoPrincipal.php';

function getPdf($pdf, $out){
    $pdf = str_replace ( ' ', '%20', $pdf );
    $fileOut = fopen($out,"wb");

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $pdf);
    curl_setopt($ch, CURLOPT_FILE, $fileOut);

    curl_exec($ch);

    $error =  curl_getinfo($ch, CURLINFO_HTTP_CODE);

    curl_close($ch);

    fclose($fileOut);
    if($error == 404){
        file_put_contents($out, '');
    }
}


$url = "http://www.policiacivil.pe.gov.br/";
$tmpfile = tempnam ("/tmp", "");

$html = file_get_html($url."index.php/criancas-desaparecidas.html");
$table =$html->find('td[valign=top] table.contentpaneopen ',1);

foreach ($table->find('td[valign=top] table[style=width: 96%; border: 0px solid #000000;] a') as $people) {

    getPdf($url.$people->href,$tmpfile);
    if (filesize($tmpfile) != 0) {
        $parser = new \Smalot\PdfParser\Parser();
        $pdf = $parser->parseFile($tmpfile);
        $text = $pdf->getText();

        $p = new Pessoa();
        $p->nome = $people->find('text',0)->_[4];
        $p->fonte = $p->imagem= str_replace ( ' ', '%20', $url.$people->href);
        $p->situacao = "Desaparecida";
        $p->estado= "PE";
        $p->data_desaparecimento = ($desaparecimento = explode("\n",explode("desde:",$text)[1])[0]);
        $p->olhos = ($desaparecimento = explode("\n",explode("Olhos",$text)[1])[0]);
        $p->pele = ($desaparecimento = explode("\n",explode("Pele",$text)[1])[0]);
        $p->datanasc = ($desaparecimento = explode("\n",explode("Nascimento",$text)[1])[0]);
        $p->dados_adicionais = "Nome da Mãe: " .($desaparecimento = explode("\n",explode("Mãe",$text)[1])[0]).
                                "Tipo Físico: ". ($desaparecimento = explode("\n",explode("Físico",$text)[1])[0]).
                                "Contato Familiares: ". ($desaparecimento = explode("\n",explode("Familiares",$text)[1])[0]);
        $p-> local_desaparecimento = str_replace("\n"," ",$desaparecimento = explode("Outras",explode("vez",$text)[1])[0]);

        atualizacao_Principal($p);

    }

    unlink($tmpfile);
}






