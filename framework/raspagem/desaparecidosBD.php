<?php
/**
 * Created by PhpStorm.
 * User: joraojr
 * Date: 22/05/18
 * Time: 11:52
 */

include 'atualizacaoPrincipal.php';
$format = "application/sparql-results+json";
$login = "admin:admin"; // login:senha
$BD1 = "http://localhost:10035/repositories/desaparecidos2";
$query = '
PREFIX foaf:<http://xmlns.com/foaf/0.1/>
PREFIX des:<http://www.desaparecidos.com.br/rdf/>  
PREFIX dbpprop:<http://dbpedia.org/property/>
SELECT  ?nome ?apelido ?data_nascimento ?sexo ?imagem ?idade ?cidade ?estado ?altura ?peso ?pele ?cor_cabelo ?cor_olho ?mais_caracteristicas 
?data_desaparecimento ?local_desaparecimento ?circunstancia_desaparecimento ?data_localizacao ?dados_adicionais ?status ?fonte 
WHERE { 
       OPTIONAL {?recurso foaf:name ?nome}.
       OPTIONAL {?recurso foaf:nick ?apelido}.
       OPTIONAL {?recurso foaf:birthday ?data_nascimento}.
       OPTIONAL {?recurso foaf:gender ?sexo}.
       OPTIONAL {?recurso foaf:img ?imagem}.
       OPTIONAL {?recurso foaf:age ?idade}.
       OPTIONAL {?recurso des:cityDes ?cidade}.
       OPTIONAL {?recurso des:stateDes ?estado}.
       OPTIONAL {?recurso dbpprop:height ?altura}.
       OPTIONAL {?recurso dbpprop:weight ?peso}.
       OPTIONAL {?recurso des:skin ?pele}.
       OPTIONAL {?recurso dbpprop:hairColor ?cor_cabelo}.
       OPTIONAL {?recurso dbpprop:eyeColor ?cor_olho}.
       OPTIONAL {?recurso des:moreCharacteristics ?mais_caracteristicas}.
       OPTIONAL {?recurso des:disappearanceDate ?data_desaparecimento}.
       OPTIONAL {?recurso des:disappearancePlace ?local_desaparecimento}.
       OPTIONAL {?recurso des:circumstanceLocation ?circunstancia_desaparecimento}.
       OPTIONAL {?recurso des:dateLocation ?data_localizacao}.
       OPTIONAL {?recurso des:additionalData ?dados_adicionais}.
       OPTIONAL {?recurso des:status ?status}.
       OPTIONAL {?recurso des:source ?fonte}.
	   FILTER (regex(?fonte,"http://www.desaparecidos.gov.br","i"))
      
} 

';

$url = urlencode($query);
$sparqlURL = $BD1.'?query='.$url;

$curl = curl_init();
//curl_setopt($curl, CURLOPT_USERPWD, $GLOBALS['login']);

curl_setopt($curl, CURLOPT_URL, $sparqlURL);

curl_setopt($curl, CURLOPT_USERPWD, $GLOBALS['login']);

curl_setopt($curl, CURLOPT_RETURNTRANSFER, true); //Recebe o output da url como uma string


curl_setopt($curl,CURLOPT_HTTPHEADER,array("Accept: ".$format ));
$resposta = curl_exec( $curl );
curl_close($curl);

$json = json_decode($resposta);
$json = $json->results->bindings;
var_dump($json);

foreach ($json  as $resposta){
    $auxPessoa = new Pessoa; //vai armazenar os dados da pessoa q estão contidos no allegro
    $auxPessoa->nome = $resposta->nome->value ; //no
    $auxPessoa->apelido = $resposta->apelido->value; //yes OK
    $auxPessoa->datanasc = $resposta->data_nascimento->value; //yes OK
    $auxPessoa->sexo = $resposta->sexo->value; //yes OK
    $auxPessoa->idade = $resposta->idade->value; //yes OK
    $auxPessoa->cidade = $resposta->cidade->value; //no
    $auxPessoa->estado = $resposta->estado->value;//yes OK
    $auxPessoa->altura = $resposta->altura->value;//yes OK
    $auxPessoa->peso = $resposta->peso->value;//yes OK
    $auxPessoa->pele = $resposta->pele->value;//yes OK
    $auxPessoa->cor_cabelo = $resposta->cor_cabelo->value;//yes OK
    $auxPessoa->cor_olho = $resposta->cor_olho->value; //yes OK
    $auxPessoa->mais_caracteristicas = $resposta->mais_caracteristicas->value; //yes OK
    $auxPessoa->data_desaparecimento = $resposta->data_desaparecimento->value; //no
    $auxPessoa->local_desaparecimento = $resposta->local_desaparecimento->value; //yes OK
    $auxPessoa->circunstancia_desaparecimento = $resposta->circunstancia_desaparecimento->value; //yOK
    $auxPessoa->data_localizacao = $resposta->data_localizacao->value; //yes OK
    $auxPessoa->dados_adicionais = $resposta->dados_adicionais->value; // yes OK
    $auxPessoa->situacao = $resposta->status->value; //yes OK
    $auxPessoa->fonte = $resposta->fonte->value; // yes OK
    atualizacao_Principal($auxPessoa);
}

