<?php
/**
 * Created by PhpStorm.
 * User: joraojr
 * Date: 10/06/18
 * Time: 04:24
 */
$format = "application/sparql-results+json";
$login = "admin:admin"; // login:senha
$BD1 = "http://localhost:10035/repositories/desaparecidos5";

$query = "# View triples
SELECT ?tenancy ?x{
  ?tenancy  <http://www.desaparecidos.com.br/rdf/moreCharacteristics> ?x .
  OPTIONAL { ?tenancy <http://www.desaparecidos.com.br/rdf/cityDes> ?o } .
  FILTER ( !bound(?o) )
}

ORDER BY ?tenancy";

$url = urlencode($query);
$sparqlURL = $BD1.'?query='.$url;

$curl = curl_init();
//curl_setopt($curl, CURLOPT_USERPWD, $GLOBALS['login']);

curl_setopt($curl, CURLOPT_URL, $sparqlURL);

curl_setopt($curl, CURLOPT_USERPWD, $GLOBALS['login']);

curl_setopt($curl, CURLOPT_RETURNTRANSFER, true); //Recebe o output da url como uma string


curl_setopt($curl,CURLOPT_HTTPHEADER,array("Accept: ".$format ));
$resposta = curl_exec( $curl );
curl_close($curl);

$json = json_decode($resposta);
$json = $json->results->bindings;
//var_dump($json);
$i = 0 ;
$j = 0;
foreach ($json as $t) {
    echo ++$j ."\n";
    $texto = $t->x->value;
    $format = 'application/json';
    $endereco = "text=" . urlencode($texto) . "&confidence=0&types=DBpedia%3APlace";
    $sparqlURL = "https://api.dbpedia-spotlight.org/pt/annotate?";
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $sparqlURL);
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($curl, CURLOPT_POST, 1);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $endereco);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array('Accept: ' . $format));
    $resposta = curl_exec($curl);
    var_dump($resposta);
    curl_close($curl);
    foreach (json_decode($resposta, true)["Resources"] as $type) {
        if (strpos($type["@types"], "DBpedia:City")) {
            $i++;
            break;
        }
    }
    sleep(1);

}

var_dump($i);
