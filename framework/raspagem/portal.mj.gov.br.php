<?php
/**
 * Created by PhpStorm.
 * User: joraojr
 * Date: 30/05/18
 * Time: 17:32
 */

namespace Facebook\WebDriver;

use Facebook\WebDriver\Chrome\ChromeOptions;
use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\Remote\RemoteWebDriver;

require_once('../vendor/autoload.php');
include 'atualizacaoPrincipal.php';

function sendPeople($driver, $place)
{
    $p = new \Pessoa();
    $p->cidade = $p->local_desaparecimento = $place;
    $p->estado = $driver->findElement(WebDriverBy::xpath("//span[@id='lblEstado']"))->getText();
    $driver->switchTo()->window($driver->getWindowHandles()[1]);
    $p->fonte = $driver->getCurrentURL();
    $p->imagem = $driver->findElement(WebDriverBy::xpath("//img"))->getAttribute("src");
    $p->nome = $driver->findElement(WebDriverBy::xpath("//span[@id='lblNome']"))->getText();
    $p->datanasc = $driver->findElement(WebDriverBy::xpath("//span[@id='lblDataNascimento']"))->getText();
    $p->altura = $driver->findElement(WebDriverBy::xpath("//span[@id='lblAltura']"))->getText();
    $p->peso = $driver->findElement(WebDriverBy::xpath("//span[@id='lblPeso']"))->getText();
    $p->cor_olho = $driver->findElement(WebDriverBy::xpath("//span[@id='lblCorOlhos']"))->getText();
    $p->cor_cabelo = $driver->findElement(WebDriverBy::xpath("//span[@id='lblCorOlhos']"))->getText();
    $p->pele = $driver->findElement(WebDriverBy::xpath("//span[@id='lblRaca']"))->getText();
    $p->circunstancia_desaparecimento = $driver->findElement(WebDriverBy::xpath("//span[@id='lblObservacao']"))->getText();
    $p->data_localizacao = $driver->findElement(WebDriverBy::xpath("//span[@id='lblDataLocalizacao']"))->getText();
    $p->dados_adicionais = "Contato: Nome: " . $driver->findElement(WebDriverBy::xpath("//span[@id='lblOrgao']"))->getText() .
        " Telefone: " . $driver->findElement(WebDriverBy::xpath("//span[@id='lblOrgao']"))->getText() .
        " E-mail: " . $driver->findElement(WebDriverBy::xpath("//span[@id='lblEmail']"))->getText();

    switch ($driver->findElement(WebDriverBy::xpath("//span[@id='lblSexo']"))->getText()) {
        case 'F':
            $p->sexo = "Feminino";
            break;
        case 'M':
            $p->sexo = "Masculino";
            break;
        default:
            break;
    };

    atualizacao_Principal($p);

    $driver->switchTo()->window($driver->getWindowHandles()[0]);

}

// start Chrome with 5 second timeout
$host = 'http://localhost:4444/wd/hub'; // this is the default
$capabilities = DesiredCapabilities::chrome();
$driver = RemoteWebDriver::create($host, $capabilities, 5000);


//$estates = array("AC", "AL", "AM", "AP", "BA", "CE", "DF", "ES", "GO", "MA", "MT", "MS", "MG", "PA", "PB", "PR", "PE", "PI", "RJ", "RN", "RO", "RS", "RR", "SC", "SE", "SP", "TO");
$estates = array("SE", "SP", "TO");
foreach ($estates as $uf) {

    $driver->get("http://portal.mj.gov.br/Desaparecidos/frmListaDesaparecidos.aspx?uf=" . strtolower($uf));

    $pages = $driver->findElements(WebDriverBy::xpath("//a[contains(@href,'doPostBack')]"));
    $x = count($pages);

    $link = $driver->findElements(WebDriverBy::xpath("//span[contains(@id, 'Label2')]"));
    $places = $driver->findElements(WebDriverBy::xpath("//span[contains(@id, 'Label9')]"));
    $k = 0;
    foreach ($link as $l) {
        $l->click();
        sendPeople($driver, $places[$k]->getText());
        $k++;
    }

    for ($i = 0; $i < $x; $i++) {
        print_r($i);
        if ($i == ($x - 1) && $pages[$i]->getText() == "...") {
            $pages[$i]->click();
            $pages = $driver->findElements(WebDriverBy::xpath("//a[contains(@href,'doPostBack')]"));
            $i = $x - intval($pages[count($pages) - 1]->getText()) + count($pages) + 1;
        } else {
            $pages[$i]->click();
            $pages = $driver->findElements(WebDriverBy::xpath("//a[contains(@href,'doPostBack')]"));

        }
        $link = $driver->findElements(WebDriverBy::xpath("//span[contains(@id, 'Label2')]"));
        $places = $driver->findElements(WebDriverBy::xpath("//span[contains(@id, 'Label9')]"));
        $k = 0;
        foreach ($link as $l) {
            $l->click();
            sendPeople($driver, $places[$k]->getText());
            $k++;
        }
    }

}
$driver->quit();
